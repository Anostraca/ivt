<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Kozí bradka</title>
</head>
<body>
<?php
$PristupOdepren = false;
$PocetNavstev = 1456;

if ($PristupOdepren) {
    echo "<br>Na tuto stránku nemáte dostatečně velké práva";
    echo "<br>Vaše IP byla zaznamenána";
} else {
    echo "<br>Vítej na našich stránkách";
    echo ++$PocetNavstev;
}
$den = 3;
if ($den = 1): echo "<br>Dnes je pondělí";
elseif ($den = 2): echo "<br>Dnes je Úterý";
elseif ($den = 3): echo "<br>Dnes je Středa";
elseif ($den = 4): echo "<br>Dnes je Čtvrtek";
elseif ($den = 5): echo "<br>Dnes je Pátek";
elseif ($den = 6): echo "<br>Dnes je Sobota";
elseif ($den = 7): echo "<br>Dnes je Neděle";
else: echo "<br>Asi jste v jiném vesmíru a nemůžu váš den zařadit.";
endif;

$dens = 1;
switch ($dens)
{
    case 1: echo "<br>Test"; break;
    case 2: echo "<br>Test"; break;
    case 3: echo "<br>Test"; break;
    case 4: echo "<br>Test"; break;
    case 5: echo "<br>Test"; break;
    case 6: echo "<br>Test"; break;
    case 7: echo "<br>Test"; break;
    default: echo "<br>Nepodařilo se zjistit den!";
}

?>
</body>
</html>